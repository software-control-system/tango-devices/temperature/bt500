//- Project : RampStateManager
//- file : RampingStateManager.h
//- author : jean coquet - SOLEIL, 2009
//- use : helper class to manage the state of an actuator (RUNNING/STANDBY)
//- REGULATION_DONE_XXX : The process value has reached the setpoint
//- 2 ways to declare the regulation done : 
//-   * the process value has crossed the setpoint 4 times without outreaching deadband
//-   * the process value has stayed in deadband during time_in_deadband seconds
//- others states : The process is trying to reach the setpoint
//- to be used with a polling thread
//- use : instanciate the class,
//- each time the thread reads the process value (e.g. the temperature) call update_temperature (my_val)
//- on a setpoint change call update_setpoint (my_new_setpoint)
//- check the REGULATION state on calling get_ramp_state ()
//- parameters are : 
//- the time_in_deadband in seconds
//- the deadband

#include "RampingStateManager.h"
#include <math.h>

const double __NAN__ = ::sqrt (-1.);

namespace rsm_ns
{
  //- CTOR ---------------------------------------------------------------------
  RampingStateMachine::RampingStateMachine (Tango::DeviceImpl * _host_device,
                                            time_t _time_in_deadband,
                                            double _deadband) : 
                                            Tango::LogAdapter (_host_device),
                                            time_in_deadband (_time_in_deadband),
                                            deadband (_deadband)
  {
		DEBUG_STREAM << "RampingStateMachine::RampingStateMachine <-" << std::endl;
    // Initialisations
	  this->temperature = 0.;
	  this->setpoint = 0.;
	  this->mem_temperature = 0.;
	  this->ramp_state_machine = REGUL_NOT_STARTED;
	  this->status = "Regul Manager is starting";
  }

  //- DTOR ---------------------------------------------------------------------
  RampingStateMachine::~RampingStateMachine ()  
  {
		DEBUG_STREAM << "RampingStateMachine::~RampingStateMachine <-" << std::endl;
    //- NOOP DTOR
  }

	//+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::update_setpoint
	*			description:		call this to set the new setpoint to the ramping class utility
	*/
	//+------------------------------------------------------------------
  void RampingStateMachine::update_setpoint (double new_setpoint)
  { 
	  DEBUG_STREAM << "RampingStateMachine::update_setpoint <-" << std::endl;
    //- std::cout << "RampingStateMachine::update_setpoint <-" << std::endl;
    setpoint = new_setpoint;
    time (&time_end); 
	  time_end += time_in_deadband;
	  time (&cur_time); 

    this->ramp_state_machine = REGUL_RUNNING;
  }

  //+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::update_temperature
	*			description:		call this to provide  the current temperature to the ramping class utility
  *     must be called cyclically by the polling thread
	*/
	//+------------------------------------------------------------------
  void RampingStateMachine::update_temperature (double current_temperature)
  { 
	  DEBUG_STREAM << "RampingStateMachine::update_temperature <-" << std::endl;
    //- std::cout << "RampingStateMachine::update_temperature current_temperature = " << current_temperature << std::endl;
    if (current_temperature != __NAN__)
      temperature = current_temperature;
    if(this->ramp_state_machine == REGUL_RUNNING ||
       this->ramp_state_machine == REGUL_1ST_SETPOINT_CROSS ||
       this->ramp_state_machine == REGUL_2ND_SETPOINT_CROSS ||
       this->ramp_state_machine == REGUL_3RD_SETPOINT_CROSS)
		{
      check_ramp_progress ();
    }
  }

	//+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::check_ramp_progress
	*			description:		checks wether the ramp is finished or not
	*/
	//+------------------------------------------------------------------
	void RampingStateMachine::check_ramp_progress(void)
	{
		DEBUG_STREAM << "RampingStateMachine::check_ramp_progress(): entering... !" << endl;
    //- std::cout << "RampingStateMachine::check_ramp_progress(): entering... !" << endl;
		//- depassement de bande morte : retour � la valeur REGUL_RUNNING
		//- overlapped the deadband
		if (fabs(this->temperature - this->setpoint) > this->deadband)
		{	
			this->ramp_state_machine = REGUL_RUNNING;
    //- std::cout << "RampingStateMachine::check_ramp_progress(): depassement deadband!" << endl;
			//- alternative : consider setpoint is reached after XX secs in the deadband :
			//- start time is when temperature is inside the deadband = setpoint +/- deadband
			time (&time_end ); 
			time_end += time_in_deadband;
			
		}
		else
		{
			//- alternative : consider setpoint is reached after XX secs in the deadband :
			//- start time is when temperature is inside the deadband = setpoint +/- deadband
			time(&cur_time);
			if(cur_time > time_end)
			{
				DEBUG_STREAM << "regulation done : " << time_in_deadband << " secs stable in deadband" << std::endl;
        //- std::cout << "regulation done : " << time_in_deadband << " secs stable in deadband" << std::endl;
				
				this->ramp_state_machine = REGUL_DONE_BY_TIME;
			}
		}
		
		switch(int(this->ramp_state_machine))
		{
		case int(REGUL_RUNNING) :
			if(is_setpoint_crossed())
				this->ramp_state_machine = REGUL_1ST_SETPOINT_CROSS;
        //- std::cout << "regulation step : REGUL_1ST_SETPOINT_CROSS" << std::endl;
			break;
		case int(REGUL_1ST_SETPOINT_CROSS) :
			if(is_setpoint_crossed())
				this->ramp_state_machine = REGUL_2ND_SETPOINT_CROSS;
        //- std::cout << "regulation step : REGUL_2ND_SETPOINT_CROSS" << std::endl;
			break;
		case int(REGUL_2ND_SETPOINT_CROSS) :
			if(is_setpoint_crossed())
				this->ramp_state_machine = REGUL_3RD_SETPOINT_CROSS;
        //- std::cout << "regulation step : REGUL_3RD_SETPOINT_CROSS" << std::endl;
			break;
		case int(REGUL_3RD_SETPOINT_CROSS) :
			if(is_setpoint_crossed())
				this->ramp_state_machine = REGUL_DONE_BY_SETPOINT_CROSS;
        //- std::cout << "regulation step : REGUL_DONE_BY_SETPOINT_CROSS" << std::endl;
			break;
		}	  
		//- memorize current temperature for next execution
		this->mem_temperature = this->temperature;
	}

	//+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::is_setpoint_crossed
	*			description:		checks setpoint crossings
	*/
	//+------------------------------------------------------------------
	bool RampingStateMachine::is_setpoint_crossed()
	{
		DEBUG_STREAM << "RampingStateMachine::is_setpoint_crossed() : entering... !" << endl;
    //- std::cout  << "RampingStateMachine::is_setpoint_crossed() : entering... !" << endl;
		if(this->setpoint >= this->mem_temperature)
		{
			if(this->temperature > this->setpoint)
			{
				DEBUG_STREAM << "RampingStateMachine::is_setpoint_crossed() : travers�e positive consigne " << endl;
        //- std::cout << "RampingStateMachine::is_setpoint_crossed() : travers�e positive consigne " << endl;
				return true;
			}
		}
		if(mem_temperature >= this->setpoint)
		{
			if(this->setpoint > this->temperature)
			{
				DEBUG_STREAM << "RampingStateMachine::is_setpoint_crossed() : travers�e negative consigne " << endl;
        //- std::cout << "RampingStateMachine::is_setpoint_crossed() : travers�e negative consigne " << endl;
				return true;
			}
		}
		DEBUG_STREAM << "RampingStateMachine::is_setpoint_crossed() : pas travers�e consigne" << endl;
    //- std::cout << "RampingStateMachine::is_setpoint_crossed() : pas travers�e consigne" << endl;
		return false;
	}

	//+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::get_state
	*			description:		checks current state
	*/
	//+------------------------------------------------------------------
	Tango::DevState RampingStateMachine::get_state(void)
	{
		DEBUG_STREAM << "RampingStateMachine::get_state() : entering... !" << endl;
    //- std::cout << "RampingStateMachine::get_state() ramp_state =" << ramp_state_machine << endl;
		
		
		//- initial state : the device is started
		if (this->ramp_state_machine == REGUL_NOT_STARTED            ||
			this->ramp_state_machine == REGUL_DONE_BY_SETPOINT_CROSS   ||
			this->ramp_state_machine == REGUL_DONE_BY_TIME)
			return Tango::STANDBY;
		else
			return Tango::RUNNING;
	}

	//+------------------------------------------------------------------
	/**
	*			internal method: RampingStateMachine::get_status
	*			description:		checks current status
	*/
	//+------------------------------------------------------------------
	std::string RampingStateMachine::get_status(void)
	{
		DEBUG_STREAM << "RampingStateMachine::get_status() : entering... !" << endl;
		std::stringstream s;
		//- initial state : the device is started
		if (this->ramp_state_machine == REGUL_NOT_STARTED)
			s << "initial state, no ramping" 
			  << std::endl;
		else if ( this->ramp_state_machine == REGUL_DONE_BY_TIME)
			s << "regulation done after " 
			<< time_in_deadband 
			<< " seconds in deadband" 
			<< std::endl;
		else if ( this->ramp_state_machine == REGUL_DONE_BY_SETPOINT_CROSS)
			s << "regulation done after 3 setpoint crosses in deadband" 
			  << std::endl;
		else if (this->ramp_state_machine == REGUL_RUNNING)
			s << "regulation running, REGUL_RUNNING, step 1/4 [" 
			<< (time_end - cur_time) 
			<< " seconds remaining before regulation success]" 
			<< std::endl;
		
		else if (this->ramp_state_machine == REGUL_1ST_SETPOINT_CROSS)
			s << "regulation running, REGUL_1ST_SETPOINT_CROSS step 2/4 [" 
			  << (time_end - cur_time) 
			  << " seconds remaining before regulation success]" 
			  << std::endl;
		
		else if( this->ramp_state_machine == REGUL_2ND_SETPOINT_CROSS)
			s << "regulation running, REGUL_2ND_SETPOINT_CROSS, step 3/4 ["  
			  << (time_end - cur_time) 
			  << " seconds remaining before regulation success]" 
			  << std::endl;
		
		else if( this->ramp_state_machine == REGUL_3RD_SETPOINT_CROSS)
			s << "regulation running,REGUL_3RD_SETPOINT_CROSS, step 4/4 ["  
			  << (time_end - cur_time) 
			  << " seconds remaining before regulation success]" 
			  << std::endl;
		else
			s << "regulation error, unknown code" 
			  << std::endl;
		this->status = s.str();
		return this->status;
	}
	
} //- namespace
