#ifndef __RAMPING_STATE_MANAGER_H__
#define __RAMPING_STATE_MANAGER_H__

//- Project : RampStateManager
//- file : RampingStateManager.h
//- author : jean coquet - SOLEIL, 2009
//- use : helper class to manage the state of an actuator (RUNNING/STANDBY)
//- REGULATION_DONE_XXX : The process value has reached the setpoint
//- 2 ways to declare the regulation done : 
//-   * the process value has crossed the setpoint 4 times without outreaching deadband
//-   * the process value has stayed in deadband during time_in_deadband seconds
//- others states : The process is trying to reach the setpoint
//- to be used with a polling thread
//- use : instanciate the class,
//- each time the thread reads the process value (e.g. the temperature) call update_temperature (my_val)
//- on a setpoint change call update_setpoint (my_new_setpoint)
//- check the REGULATION state on calling get_ramp_state ()
//- parameters are : 
//- the time_in_deadband in seconds
//- the deadband


#include <tango.h>

namespace rsm_ns
{
//- state machine for ramping
	//- used to determine wether the device is ramping to reach the setpoint
	//- used by state and status
	//- ramp_state_machine = 0 : ramping
	//- ramp_state_machine = 1 : setpoint is not reached
	//- ramp_state_machine = 2 : 1rst cross with setpoint encountered
	//- ramp_state_machine = 3 : 2nd  cross with setpoint encountered (did not overlapped the deadband)
	//- ramp_state_machine = 4 : 3rd  cross with setpoint encountered (did not overlapped the deadband)
	//- state /status is MOVING for 0 to 3 and STANDBY for 4

  typedef enum
  {
	  REGUL_NOT_STARTED = 0,
    REGUL_RUNNING,
    REGUL_1ST_SETPOINT_CROSS,
    REGUL_2ND_SETPOINT_CROSS,
    REGUL_3RD_SETPOINT_CROSS,
    REGUL_DONE_BY_SETPOINT_CROSS,
	  REGUL_DONE_BY_TIME
  } RampStateMachine;



  class RampingStateMachine : public Tango::LogAdapter
  {
  public : 
    //- CTOR
    RampingStateMachine ( Tango::DeviceImpl * _host_device,
                          time_t _time_in_deadband = 60,
                          double _deadband = 1.);
    //- DTOR
		virtual ~RampingStateMachine(void);

    //- must be called cyclically at each hardware reading of the temperature by the polling thread
    void update_temperature (double current_temperature);

    //- must be done at set setpoint change
    void update_setpoint (double new_setpoint);

    //- returns the ramp State Machine State
    Tango::DevState get_state (void);

    //- the way to get the status
    std::string get_status(void);

    //- stop the regulation state manager (return to STANDBY State)
    void stop (void)
    {
      //- std::cout << "RampingStateMachine::stop () <-" << std::endl;
      ramp_state_machine = REGUL_NOT_STARTED;
    };

    //- deadband changes
    void set_deadband (double deadb)
    {
      deadband = deadb; 
    };
    //- acces to deadband
    double get_deadband (void)
    {
      return deadband;
    };


  private : 
	  //- utilities for ramping
		//- internal purpose temperature and setpoint recopy
    double temperature;
    double mem_temperature;
    double setpoint;

    //- the deadband is used to determine if setpoint is reached
    double deadband;
    //- the time in deadband
    time_t time_in_deadband;

    //- utilities to control the state of the regulation
    RampStateMachine ramp_state_machine;
	  time_t time_end;
    time_t cur_time;

	  //- chek ramp progress and setpoint is crossed
	  void check_ramp_progress(void);
	  bool is_setpoint_crossed(void);

    std::string status;

  };
} // namespace rsp_ns
#endif //-__RAMPING_STATE_MANAGER_H__



