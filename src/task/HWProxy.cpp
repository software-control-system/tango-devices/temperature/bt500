
//- Project : BT500
//- file : HWProxy.cpp

#include <task/HWProxy.h>
#include <math.h>
#include "StringTokenizer.h"
#include <iomanip>


namespace BT500_ns
{
  // ============================================================================
  // Some defines and constants
  // ============================================================================
  static const double __NAN__ = ::sqrt(-1.);


  // ============================================================================
  // Config::Config
  // ============================================================================
  HWProxy::Config::Config ()
  {
    url                 = "Not Initialised";
    startup_timeout_ms  = 2000;
    read_timeout_ms     = 1000;
    periodic_timeout_ms = 1000;
    time_in_deadband    = 60;
    channel_in_use      = 0x0F;
    }

  HWProxy::Config::Config (std::string _url) : 
    url (_url)
  {
    //- NOOP CTOR
  }
  HWProxy::Config::Config (const Config & _src)
  {
    *this = _src;
  }
  // ============================================================================
  // DBConfig::operator =
  // ============================================================================
  void HWProxy::Config::operator = (const Config & _src)
  {
    url                 = _src.url;
    startup_timeout_ms  = _src.startup_timeout_ms;
    read_timeout_ms     = _src.read_timeout_ms;
    periodic_timeout_ms = _src.periodic_timeout_ms;
    startup_timeout_ms  = _src.startup_timeout_ms;
    time_in_deadband    = _src.time_in_deadband;
    channel_in_use      = _src.channel_in_use;
  }
  //-----------------------------------------------
  //- Ctor ----------------------------------------
  HWProxy::HWProxy (Tango::DeviceImpl * _host_device, 
                    Config & _conf) : 
              yat4tango::DeviceTask(_host_device),
              conf (_conf),
              host_dev (_host_device),
              setpoint (__NAN__),
              temperature_1 (__NAN__),
              temperature_2 (__NAN__),
              temperature_3 (__NAN__),
              temperature_4 (__NAN__)
  {
    DEBUG_STREAM << "HWProxy::HWProxy <- " << std::endl;

    //- yat::Task configure optional msg handling
    this->set_timeout_msg_period (conf.read_timeout_ms);
    this->enable_timeout_msg(true);
    this->enable_periodic_msg(true);
    this->set_periodic_msg_period(this->conf.periodic_timeout_ms);
    this->serial = 0;
    bt500_state = BT_INIT_GOING_TO_REG_PAGE;
    this->com_state =HWP_NO_ERROR;
    this->pump_automatic_mode = false;
    this->pump_speed = 0;
    this->regulation_channel = 1;
    this->oven_max_power = 0;
    this->oven_power_current_percent = 0;

    this->command_ok = 0;
    this->command_error = 0;
    this->consecutive_command_error = 0;

    try
    {
      DEBUG_STREAM << "HWProxy::HWProxy trying to allocate Regulation Manager" << std::endl;
      reg_manager = new rsm_ns::RampingStateMachine (host_dev, conf.time_in_deadband, 1);

    }
    catch (...)
    {
      this->com_state = HWP_INITIALIZATION_ERROR;
      this->last_error = "Error trying to allocate Ramp State Manager\n"; 
      ERROR_STREAM << "HWProxy::HWProxy Error trying to allocate Ramp State Manager" << std::endl; 
    }
  }


  //-----------------------------------------------
  //- Dtor ----------------------------------------
  HWProxy::~HWProxy (void)
  {
    DEBUG_STREAM << "HWProxy::~HWProxy <- " << std::endl;
    if (serial)
    {
      delete serial;
      serial = 0;
    }

    if (reg_manager)
    { 
      delete reg_manager;
      reg_manager = 0;
    }

  }

  //-----------------------------------------------
  //- the user core of the Task -------------------
  void HWProxy::process_message (yat::Message& _msg) throw (Tango::DevFailed)
  {
    //- The DeviceTask's lock_ -------------

    DEBUG_STREAM << "HWProxy::handle_message::receiving msg " << _msg.to_string() << std::endl;

    //- handle msg
    switch (_msg.type())
    {
      //- THREAD_INIT =======================
    case yat::TASK_INIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message::THREAD_INIT::thread is starting up" << std::endl;
        //- "initialization" code goes here
        last_error = "No Error\n";

        //- create the Device Proxy
        DEBUG_STREAM << " HWProxy::handle_message::THREAD_INIT try to get device proxy " << std::endl;
        this->create_device_proxy ();

        //- initialise some stuff 
        this->command_source = BT_STATE;

/*        //- check the state of the BT500 (read the STA? value)
        this->check_BT_state ();
        if (bt500_state != BT_PAP_REG_STATE)
        {
          yat::Message * msg = new yat::Message(INIT_GO_TO_RGU);
          if (msg == 0)
          {
            ERROR_STREAM << "BT500::HWProxy::TASK_INIT error trying to create msg "<< endl;
            Tango::Except::throw_exception (_CPTC ("OUT_OF_MEMORY"),
                                            _CPTC ("yat::Message allocation failed"),
                                            _CPTC ("BT500::HWProxy::TASK_INIT"));
          }
          this->post(msg);
        }
*/
      } 
      break;

      //- TASK_EXIT =======================
    case yat::TASK_EXIT:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_EXIT thread is quitting" << std::endl;

        //- "release" code goes here
      }
      break;

      //- TASK_PERIODIC ===================
    case yat::TASK_PERIODIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling TASK_PERIODIC msg com state = " 
                     << get_com_status () 
                     << " : BT500 State : " 
                     << "[" 
                     << bt500_state_str [this->bt500_state] 
                     << "]" 
                     << std::endl;
        //- code relative to the task's periodic job goes here
        if (com_state == HWP_INITIALIZATION_ERROR ||
            com_state == HWP_DEVICE_PROXY_ERROR ||
            com_state == HWP_COMMUNICATION_ERROR)
        {
          ERROR_STREAM << "HWProxy::handle_message::TASK_PERIODIC : ERROR [" << get_com_status () << "]" << std::endl;
          return;          
        }
        else
        {
          //- check the BT State STA? cmd
          if (bt500_state != BT_PAP_REG_STATE)
          {
            INFO_STREAM << "HWProxy::handle_message::TASK_PERIODIC : NOT IN REG STATE" << std::endl;
            //- std::cout << "HWProxy::handle_message::TASK_PERIODIC : NOT IN REG STATE" << std::endl;
            bt500_state = check_BT_state ();
            //- do not stop as it could be in other states ans stop () stops the ramping state manager
            //- reg_manager->stop ();
          }
          else
          {
            INFO_STREAM << "HWProxy::handle_message::TASK_PERIODIC : BT500 STATE = " << bt500_state_str[bt500_state] << std::endl;
            //- get the temperatures
            this->poll_hardware ();
          }
        }
      }
      break;

      //- TASK_TIMEOUT ===================
    case yat::TASK_TIMEOUT:
      {
        //- code relative to the task's tmo handling goes here
        ERROR_STREAM << "HWProxy::handle_message handling TASK_TIMEOUT msg" << std::endl;
        this->last_error = "HWProxy::handle_message received TASK_TIMEOUT msg\n";
      }
      break;
      //- USER_DEFINED_MSG ================

    case PAP_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling PAP_MSG msg" << std::endl;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          this->write_read ("PAP \r", response);
        }
      }
      break;

    case REG_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling REG_MSG msg" << std::endl;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
        this->write_read ("REG \r", response);
        }
      }
      break;

    case INIT_GO_TO_RGU:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling INIT_GO_TO_RGU msg" << std::endl;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          DEBUG_STREAM << "HWProxy::handle_message handling INIT_GO_TO_RGU msg before write_read" << std::endl;
          this->write_read ("RGU \r", response);
        }
      }
      break;

    case RET_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling RET_MSG msg" << std::endl;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          this->write_read ("RET \r", response);
        }
      }
      break;

    case FIN_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling FIN_MSG msg" << std::endl;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          this->write_read ("FIN \r", response);
        }
      }
      break;

    case SET_SETPOINT_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_SETPOINT_MSG msg" << std::endl;
        double * double_value = 0;
        bool wr_ok = false;
        _msg.detach_data(double_value);
        if (double_value) 
        {
          std::string response;
          std::stringstream cmd;
          cmd << "TCO=" 
            << std::setw (7)
              << std::showpoint
              << std::fixed
              << std::setprecision (3) 
              << std::setfill ('0')
              << (*double_value) 
              << "\r" 
              << std::ends;
          { //- enter critical section
            yat::AutoMutex<> guard(this->m_lock);
            wr_ok = this->write_read (cmd.str (), response);
          }
          //- update the regulation utility class with the new setpoint
          if (wr_ok)
            reg_manager->update_setpoint ((*double_value));
        }
      }
      break;

    case SET_DEADBAND_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_DEADBAND_MSG msg" << std::endl;
        double * double_value = 0;
        bool wr_ok = false;
        _msg.detach_data(double_value);
        if (double_value) 
        {
          reg_manager->set_deadband (*double_value);
        }
      }
      break;

    case STOP_RAMP_MSG:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling STOP_RAMP_MSG msg" << std::endl;
        //- std::cout << "HWProxy::handle_message handling STOP_RAMP_MSG msg" << std::endl;
        reg_manager->stop ();
      }
      break;

    case SET_PUMP_MANUAL:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_PUMP_MANUAL msg" << std::endl;
        bool wr_ok = false;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          wr_ok = this->write_read (std::string ("VAT=0\r"), response);
        }
      }
      break;

    case SET_PUMP_AUTOMATIC:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_PUMP_AUTOMATIC msg" << std::endl;
        bool wr_ok = false;
        std::string response;
        { //- enter critical section
          yat::AutoMutex<> guard(this->m_lock);
          wr_ok = this->write_read (std::string ("VAT=1\r"), response);
        }
      }
      break;

    case SET_PUMP_SPEED:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_PUMP_SPEED msg" << std::endl;
        int * int_value = 0;
        bool wr_ok = false;
        _msg.detach_data(int_value);
        if (int_value && (*int_value) >= 0 && (*int_value) <=32 && !this->pump_automatic_mode) 
        { //- enter critical section
          std::string response;
          yat::AutoMutex<> guard(this->m_lock);
          std::stringstream s;
          s << "DPO="
            << *int_value
            << "\r"
            << std::ends;
          wr_ok = this->write_read (s.str (), response);
        }
      }
      break;

    case SET_OVEN_MAX_POWER:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_OVEN_MAX_POWER msg" << std::endl;
        int * int_value = 0;
        bool wr_ok = false;
        _msg.detach_data(int_value);

        if (int_value && (*int_value) >= 0 && (*int_value) <=100) 
        { //- enter critical section
          std::string response;
          yat::AutoMutex<> guard(this->m_lock);
          std::stringstream s;
          s << "PUI="
            << *int_value
            << "\r"
            << std::ends;
          wr_ok = this->write_read (s.str (), response);
        }
      }
      break;

    case SET_REGUL_CHANNEL:
      {
        DEBUG_STREAM << "HWProxy::handle_message handling SET_REGUL_CHANNEL msg" << std::endl;
//- std::cout << "HWProxy::SET_REGUL_CHANNEL : channels in use binary : " << conf.channel_in_use << std::endl;

        int * int_value = 0;
        bool wr_ok = false;
        _msg.detach_data(int_value);
        if (int_value && (*int_value) >= 1 && (*int_value) <= 4)
        {
          if ((*int_value == 1 && (conf.channel_in_use & 1) != 0) ||
              (*int_value == 2 && (conf.channel_in_use & 2) != 0) ||
              (*int_value == 3 && (conf.channel_in_use & 4) != 0) ||
              (*int_value == 4 && (conf.channel_in_use & 8) != 0))
          { //- enter critical section
            std::string response;
            yat::AutoMutex<> guard(this->m_lock);
            std::stringstream s;
            s << "CCO="
              << *int_value
              << "\r"
              << std::ends;
            wr_ok = this->write_read (s.str (), response);
          }
          else
          {
            this->last_error = "error trying to change regulation channel to a non valid channel [check property channelInUse]\n";
          }
        }
      }
      break;
    default:
  		  ERROR_STREAM<< "SublimationManagerTask::handle_message::unhandled msg type received" << std::endl;
  		break;
    } //- switch (_msg.type())
  } //- HWProxy::process_message

  //-----------------------------------------------
  //- Access to temperatures -----------------------
  //-----------------------------------------------
  double HWProxy::get_temperature ( CommandSource cmd_source)
  {
    DEBUG_STREAM << "HWProxy::get_temperature <-" << std::endl;
    if(consecutive_com_errors > MAX_COM_RETRIES)
    {
      this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "HWProxy::get_temperature last error : max consecutive comm errors retries limit reached\n";
    }
    switch (cmd_source)
    {
    case TEMP_SOURCE_SP : 
      return setpoint;
      break;
    case TEMP_SOURCE_TC1 :
      return temperature_1;
      break;
    case TEMP_SOURCE_TC2 :
      return temperature_2;
      break;
    case TEMP_SOURCE_TC3 :
      return temperature_3;
      break;
    case TEMP_SOURCE_TC4 :
      return temperature_4;
      break;
    default : return __NAN__;
    }
  }


  //-----------------------------------------------
  //- create serial device_proxy ------------------
  //-----------------------------------------------
  void HWProxy::create_device_proxy (void)
  {
    try
    {
      DEBUG_STREAM << " HWProxy::create_device_proxy try to get device proxy on  " << this->conf.url << std::endl;
      if (serial)
      {
        delete serial;
        serial = 0;
      }

      serial = new Tango::DeviceProxyHelper (conf.url, host_dev);
      if (serial == 0)
        throw std::bad_alloc ();

      this->com_error = 0;
      this->consecutive_com_errors = 0;
      this->com_success = 0;
      this->com_state =HWP_NO_ERROR;
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << " HWProxy::create_device_proxy exception caugth trying to get device proxy on " << conf.url << std::endl;
      this->last_error = " exception caugth trying to get device proxy on " + conf.url;
      this->last_error += "\n";
      this->last_error += "Tango exception description : ";
      this->last_error += static_cast<const char*>(e.errors[0].desc);
      this->last_error += "\n";
      this->com_state = HWP_DEVICE_PROXY_ERROR;
      return;
    }
    catch (bad_alloc)
    {
      ERROR_STREAM << " HWProxy::create_device_proxy bad_alloc caugth trying to get device proxy " << std::endl;
      this->last_error = " bad_alloc caugth trying to get device proxy\n";
      this->com_state = HWP_DEVICE_PROXY_ERROR;
      return;
    }
    catch(...)
    {
      ERROR_STREAM << " HWProxy::create_device_proxy exception caugth trying to get device proxy on " << conf.url << std::endl;
      this->last_error = " exception caugth trying to get device proxy on " + conf.url;
      this->last_error += "\n";
      this->com_state = HWP_DEVICE_PROXY_ERROR;
      return;
    }
    try
    {
      (*serial)->ping ();
    }
    catch (Tango::DevFailed &e)
    {
      ERROR_STREAM << " HWProxy::create_device_proxy exception caugth trying to pnig device " << conf.url << std::endl;
      this->last_error = " exception caugth trying to ping device " + conf.url;
      this->last_error += "\n";
      this->last_error += "Tango exception description : ";
      this->last_error += static_cast<const char*>(e.errors[0].desc);
      this->last_error += "\n";
      this->com_state = HWP_DEVICE_PROXY_ERROR;
      return;
    }
  }

  //-----------------------------------------------
  //- poll_hardware
  //- Cyclic read of setpoint to TC4
  //-----------------------------------------------
  void HWProxy::poll_hardware (void)
  {
    DEBUG_STREAM << "HWProxy::poll_hardware trying to read BT500" << std::endl;

    std::string cmd;
    std::string response;
    bool rw_ok = false;

    switch (command_source)
    {
    case BT_STATE : 
      check_BT_state ();
      ++command_source;
      break;
    case TEMP_SOURCE_SP : 
      cmd = "TCO?\r";
      this->setpoint = get_temp_value (cmd);
      ++command_source;
      break;
    case TEMP_SOURCE_TC1 :
      ++command_source;
      if ((conf.channel_in_use & 0x01) == 0)
      {
        temperature_1 = __NAN__;
        return;
      }
      cmd = "TC1?\r";
      temperature_1 = get_temp_value (cmd);
      //- update the regulation utility class
      if (regulation_channel == 1)
        reg_manager->update_temperature (temperature_1);
    break;
    case TEMP_SOURCE_TC2 : 
      ++command_source;
      //- read only if channel is in use
      if ((conf.channel_in_use & 2) == 0)
      {
        temperature_2 = __NAN__;
        return;
      }
      cmd = "TC2?\r";
      temperature_2= get_temp_value (cmd);
      if (regulation_channel == 2)
        reg_manager->update_temperature (temperature_2);
      break;
    case TEMP_SOURCE_TC3 : 
      ++command_source;
      //- read only if channel is in use
      if ((conf.channel_in_use & 4) == 0)
      {
        temperature_3 = __NAN__;
        return;
      }
      cmd = "TC3?\r";
      temperature_3 = get_temp_value (cmd);
      if (regulation_channel == 3)
        reg_manager->update_temperature (temperature_3);
    break;
    case TEMP_SOURCE_TC4 : 
      ++ command_source;
      //- read only if channel is in use
      if ((conf.channel_in_use & 8) == 0)
      {
        temperature_4 = __NAN__;
        return;
      }
      cmd = "TC4?\r";
      temperature_4 = get_temp_value (cmd);
      if (regulation_channel == 4)
        reg_manager->update_temperature (temperature_4);
      break;

    case BT_REGULATION_CHANNEL : 
      cmd = "CCO?\r";
      regulation_channel = static_cast <int>(get_temp_value (cmd));
      ++ command_source;
      break;

    case PUMP_MAN_AUT : 
      {
        //- //- std::cout << "check pump [manual|automatic]..." << std::endl;
        std::string resp;
        cmd = "VAT?\r";
        write_read (cmd, resp);
        if (resp.find ("0") != std::string::npos)
        {
          //- //- std::cout << "pump in manual mode" << std::endl;
          this->pump_automatic_mode = false;
        }
        else
        {
          //- //- std::cout << "pump in automatic mode" << std::endl;
          this->pump_automatic_mode = true;
        }
        ++ command_source;
      }
      break;

    case OVEN_POWER_PERCENT : 
      cmd = "PPF?\r";
      oven_power_current_percent = static_cast <int>(get_temp_value (cmd));
      ++ command_source;
      break;

    case OVEN_POWER_MAX : 
      cmd = "PUI?\r";
      oven_max_power = static_cast <int>(get_temp_value (cmd));
      ++ command_source;
      break;

    case PUMP_MAN_SPEED : 
      cmd = "DPO?\r";
      pump_speed = static_cast <int>(get_temp_value (cmd));
      command_source = BT_STATE;
      break;

    default : 
      command_source = BT_STATE;
    }
  }
  //-----------------------------------------------
  //- get_temp_value
  //- send command return result
  //-----------------------------------------------
  double HWProxy::get_temp_value (std::string cmd)
  {

    DEBUG_STREAM << "HWProxy::get_temp_value <- " << std::endl;

    std::string response;
    { //- enter critical section
      yat::AutoMutex<> guard(this->m_lock);
      bool rw_ok = write_read (cmd, response);
    }

    //- convert the temperature
    StringTokenizer tok ("\r", response);
    DEBUG_STREAM << "HWProxy::get_temp_value nb of tokens : " << tok.get_number_of_token () <<  std::endl;

//-std::cout << "HWProxy::get_temp_value nb of token = " << tok.get_number_of_token () << std::endl;

if (tok.get_number_of_token () < 2 || response.find ("NO") != std::string::npos)
    {
      ERROR_STREAM << "HWProxy::get_temp_value command [" << cmd << "] failed response error [" << response << "] expected something like [<CR>10.000><CR>]" << std::endl;
      this->last_error = "command [" + cmd + "] failed response error [" + response +  "] expected something like [<CR>10.000><CR>]\n";
      //- this->com_error ++;
      if (++ this->consecutive_com_errors > MAX_COM_RETRIES)
        this->com_state = HWP_COMMUNICATION_ERROR;
      return __NAN__;
    }
    double tmp;
    try
    {
      tmp = tok.get_token <double> (0);
      DEBUG_STREAM << "HWProxy::get_temp_value token 0 = " << tok.get_token <double> (0) << std::endl;
      return tmp;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::get_temp_value tokenize error token 0 = " << tok.get_token <std::string> (0) << std::endl;
      this->last_error = "response error [" + response +  "] expected something like [<CR>10.000><CR>]\n";
      //- this->com_error ++;
      if (++ this->consecutive_com_errors > MAX_COM_RETRIES)
        this->com_state = HWP_COMMUNICATION_ERROR;
      return __NAN__;
    }
  }
  //-----------------------------------------------
  //- write_read
  //- send command return result
  //-----------------------------------------------
  bool HWProxy::write_read (std::string cmd, std::string & response)
  {
    DEBUG_STREAM << "HWProxy::write_read <- for command [" << cmd << "]" << std::endl;
    //- case of  device proxy error
    if (this->com_state == HWP_DEVICE_PROXY_ERROR)
    { std::cout << "**************erreur DEVICE_PROXY*********************" << std::endl;
      return false;
    }

    Tango::DevLong nb_char_written = 0;
    Tango::DevLong nb_char_read_back = 0;
    std::string tmp;

    bool finished = false;
    std::stringstream s;
    s << "HWProxy::write_read cmd [";
    for (size_t i = 0; i < cmd.size (); i++)
    {
      if (cmd[i] > 31) 
        s << cmd[i];
     else 
      s << "(" << static_cast<int> (cmd[i]) << ")" ;
    }
    s << "]" << endl << endl;
    INFO_STREAM << "HWProxy::write_read for command " << s.str ();

    try
    {
      //- clean the buffers
      this->serial->command_in("DevSerFlush", Tango::DevLong(2));

      //- send the command
      this->serial->command_inout("DevSerWriteString", cmd.c_str (), nb_char_written);

      response.clear ();
      //- read in a loop until the chars ">\r" 
      yat::Timestamp start, now;
      _GET_TIME(start);
      do
      {
        tmp.clear ();
        //- sleep 50 ms before getting reading
        omni_thread::sleep (0, 10000000);

        //- how much in the buffer?
        this->serial->command_out("DevSerGetNChar", nb_char_read_back);
        //- get them in a temp DVCA 
        if (nb_char_read_back > 0)
        {
          this->serial->command_inout((const char *)"DevSerReadNChar", nb_char_read_back, tmp);
          response += tmp;

          DEBUG_STREAM << "HWProxy::write_read response : [" << response << "]" << std::endl;
          if (tmp.find (">") != std::string::npos )
          {
            DEBUG_STREAM << "HWProxy::write_read end of response string [>] received" << std::endl;
            finished = true;
            break;
          }
        }

        //- timeout
        _GET_TIME(now);
        if (_TMO_EXPIRED(start, now, (static_cast<double>(conf.read_timeout_ms) / 1000.)))
        {
          ERROR_STREAM << "HWProxy::write_read Timeout on response" << std::endl;
          this->com_error ++;
          if (++this->consecutive_com_errors > MAX_COM_RETRIES)
            this->com_state = HWP_COMMUNICATION_ERROR;
          this->last_error = "last error : timeout occured trying to read HW\n";
          finished = true;
        }
      }while (!finished);
    }
    catch(...)
    {
      ERROR_STREAM << "HWProxy::write_read catched Exception trying to read the response" << std::endl;
      this->com_error ++;
      if (++this->consecutive_com_errors > MAX_COM_RETRIES)
        this->com_state = HWP_COMMUNICATION_ERROR;
      this->last_error = "last error : exception occured trying to read HW\n";
      return false;
    }
    //- log the response
    std::stringstream r;
    r << " response received [";
    for (size_t i = 0; i < response.size (); i++)
    {
      if (response[i] > 31) 
        r << response[i];
     else 
      r << "(" << static_cast<int> (response[i]) << ")" ;
    }
    r << "]" << endl << endl;

    INFO_STREAM << "HWProxy::write_read cmd [" << cmd << "] OK response " << r.str () << std::endl;

    this->com_state = HWP_NO_ERROR;
    this->com_success ++;
    this->consecutive_com_errors = 0;

    if (response.find ("NO") != std::string::npos)
    {
      ERROR_STREAM << "HWProxy::write_read Command Refused By BT500 response : " << response << std::endl;
      this->last_error = "HWProxy::write_read response refused by BT500 response : " + response;
      this->last_error += "\n";
      ++command_error;
      ++consecutive_command_error;
      if (consecutive_command_error > MAX_COMMAND_RETRIES)
      {
        this->com_state = HWP_COMMAND_ERROR;
        this->bt500_state = BT_UNKNOWN;
      }

      this->last_error = "last error : MAX Consecutive Command Errors reached\n";
      
    }
    else
    {
      ++this->command_ok;
      consecutive_command_error = 0;
    }
    return true;
  }

  //-----------------------------------------------
  //- check_BT_state
  //- send command return result
  //-----------------------------------------------
  BT500State HWProxy::check_BT_state ()
  {
    std::string response;
    bool rw_ok = false;

    DEBUG_STREAM << "HWProxy::check_BT_state <-" << std::endl;
    { //- enter critical section
      yat::AutoMutex<> guard(this->m_lock);
      std::string cmd ("STA?\r");
      cmd.append ("\0");
      rw_ok = write_read (cmd, response);
    }
    if (!rw_ok)
    {
      ERROR_STREAM << "HWProxy::check_BT_state communication failed with BT500" << std::endl;
      this->last_error = "Error occured trying to get the BT500 Status\n";
      this->com_state = HWP_COMMUNICATION_ERROR;
      return BT_UNKNOWN;
    }
    if (response.size () == 0)
    {
      ERROR_STREAM << "HWProxy::check_BT_state No Response From BT500" << std::endl;
      this->last_error = "HWProxy::check_BT_state NO BT500 response (TimeOut? BT500 in LOCAL?)\n";
      this->com_state = HWP_COMMUNICATION_ERROR;
      return BT_UNKNOWN;
    }
    
    if (response.find ("NO") != std::string::npos)
    {
      ERROR_STREAM << "HWProxy::check_BT_state Command STA? Refused By BT500 response : " << response << std::endl;
      this->last_error = "HWProxy::check_BT_state Command STA? refused by BT500 response : " + response;
      return BT_UNKNOWN;
    }
   
    StringTokenizer tok ("\r", response);
    DEBUG_STREAM << "HWProxy::check_BT_state nb of tokens : " << tok.get_number_of_token () <<  std::endl;

    if (tok.get_number_of_token () < 2)
    {
      ERROR_STREAM << "HWProxy::check_BT_state command STA? failed response error [" << response << "] expected something like [.1.OK>.]" << std::endl;
      this->last_error = "command STA? failed response error [" + response +  "] expected something like [.1.OK>.]\n";
      this->com_state = HWP_HARDWARE_ERROR;
      return BT_UNKNOWN;
    }

    this->sta_value = BT_UNKNOWN;
    try
    {
      this->sta_value = tok.get_token <int> (0);
      DEBUG_STREAM << "HWProxy::check_BT_state token 0 = " << tok.get_token <int> (0) << std::endl;
    }
    catch (...)
    {
      ERROR_STREAM << "HWProxy::check_BT_state tokenize error token 0 = " << tok.get_token <std::string> (0) << std::endl;
      this->last_error = "response error [" + response +  "] expected something like [<CR>xxxOK><CR>]\n";
      ++command_error;
      ++consecutive_command_error;
      return BT_UNKNOWN;
    }
    //- std::cout << "HWProxy::check_BT_state STA = " << this->sta_value << std::endl;

    switch (this->sta_value)
    {
      case 1 :
        this->bt500_state = BT_MAIN_STATE;
        break;
      case 11 : 
        this->bt500_state = BT_AUT_STATE;
        break;
      case 12 : 
        this->bt500_state = BT_PAP_STATE;
        break;
      case 111 : 
        this->bt500_state = BT_AUT_EDT_STATE;
        break;
      case 112 : 
        this->bt500_state = BT_AUT_REG_STATE;
        break;
      case 113 : 
        this->bt500_state = BT_AUT_UAF_STATE;
        break;
      case 121 : 
        this->bt500_state = BT_PAP_PID_STATE;
        break;
      case 122 : 
        this->bt500_state = BT_PAP_STP_STATE;
        break;
      case 12300 : 
        this->bt500_state = BT_OVEN_OFF;
        break;
      case 12310 : 
        this->bt500_state = BT_PAP_REG_STATE;
        break;
      case 12301 : 
      case 12311 : 
        this->bt500_state = BT_TEMP_AT_ZERO;
        break;
      case 12302 : 
      case 12312 : 
        this->bt500_state = BT_CLOCK_FAULT;
        break;
      case 12303 : 
      case 12313 : 
        this->bt500_state = BT_SENSOR_FAULT;
        break;
      case 12304 : 
      case 12314 : 
        this->bt500_state = BT_TEMP_ALARM;
        break;
      case 12305 : 
      case 12315 : 
        this->bt500_state = BT_OVEN_RESISTOR_FAULT;
        break;
      case 12306 : 
      case 12316 : 
        this->bt500_state = BT_OVEN_POWER_REGULATION_FAULT;
        break;
      case 124 : 
        this->bt500_state = BT_AUT_REG_DRE_STATE;
        break;
      case 1121 : 
        this->bt500_state = BT_AUT_REG_NCO_STATE;
        break;
      case 1122 : 
        this->bt500_state = BT_MAIN_STATE;
        break;
      case 1123 : 
        this->bt500_state = BT_OVEN_OFF;
        break;
      default :
        this->bt500_state = BT_UNKNOWN;
    }

    DEBUG_STREAM << "HWProxy::check_BT_state bt500 status = " << bt500_state_str[bt500_state] << std::endl;
    return bt500_state;
  }


  //-----------------------------------------------
  //- exec_low_level_command
  //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
  //-----------------------------------------------
  std::string HWProxy::exec_low_level_command (std::string cmd)
  {
    DEBUG_STREAM << "HWProxy::exec_low_level_command for command [" << cmd << "\n]" << std::endl;

    cmd.append ("\r");
    cmd.append ("\0");
    std::string response;
    bool rw_ok = false;
    { //- enter critical section
      DEBUG_STREAM << "HWProxy::exec_low_level_command CRITICAL SECTION <-" << std::endl;
      yat::AutoMutex<> guard(this->m_lock);
      rw_ok = write_read (cmd, response);
      DEBUG_STREAM << "HWProxy::exec_low_level_command CRITICAL SECTION ->" << std::endl;
    }
    if (!rw_ok)
    {
      this->last_error = "Error occured trying to exec the low level command " + cmd;
      return std::string ("COM ERROR");
    }

    return response;
  }


} //- namespace
