
//- Project : BT500
//- file : HWProxy.h
//- threaded reading of the HW


#ifndef __HW_PROXY_H__
#define __HW_PROXY_H__

#include <tango.h>
#include <yat4tango/DeviceTask.h>
#include <yat/time/Timer.h>
#include <DeviceProxyHelper.h>
#include "RampingStateManager.h"


namespace BT500_ns
{
  // ============================================================================
  // some defines enums and constants
  // ============================================================================
  //- startup and communication errors-------------------------------------------
  const size_t MAX_COM_RETRIES = 20;
  const size_t MAX_COMMAND_RETRIES = 5;


  typedef enum
  {
    HWP_UNKNOWN_ERROR        = 0,
    HWP_INITIALIZATION_ERROR,
    HWP_DEVICE_PROXY_ERROR,
    HWP_COMMUNICATION_ERROR,
    HWP_COMMAND_ERROR,
    HWP_HARDWARE_ERROR,
    HWP_SOFTWARE_ERROR,
    HWP_NO_ERROR
  } ComState;  


  //- the status strings for ComState
  const size_t HWP_STATE_MAX_SIZE = 8;
  static const std::string hw_state_str[HWP_STATE_MAX_SIZE] =
  {
    "Unknown Error",
    "Initialisation Error",
    "Device Proxy Error",
    "Communication Error",
    "Command Error",
    "Hardware Error",
    "Software Error",
    "Communication Running"
  };

  //- the current command to send
  typedef enum
  {
    BT_STATE = 0,
    TEMP_SOURCE_SP,
    TEMP_SOURCE_TC1,
    TEMP_SOURCE_TC2,
    TEMP_SOURCE_TC3,
    TEMP_SOURCE_TC4,
    BT_REGULATION_CHANNEL,
    PUMP_MAN_AUT,
    OVEN_POWER_PERCENT,
    OVEN_POWER_MAX,
    PUMP_MAN_SPEED
  } CommandSource;

  typedef enum _BT500State
  {
    BT_UNKNOWN = 0,
    BT_MAIN_STATE,
    BT_AUT_STATE,
    BT_PAP_STATE,
    BT_AUT_EDT_STATE,
    BT_AUT_REG_STATE,
    BT_AUT_UAF_STATE,
    BT_PAP_PID_STATE,
    BT_PAP_STP_STATE,
    BT_PAP_REG_STATE,
    BT_PAP_UAF_STATE,
    BT_AUT_REG_DRE_STATE,
    BT_AUT_REG_NCO_STATE,
    BT_OVEN_OFF,
    BT_TEMP_AT_ZERO,
    BT_CLOCK_FAULT,
    BT_SENSOR_FAULT,
    BT_TEMP_ALARM,
    BT_OVEN_RESISTOR_FAULT,
    BT_OVEN_POWER_REGULATION_FAULT,
    BT_INIT_GOING_TO_REG_PAGE
  } BT500State;



  static const std::string bt500_state_str[21] =
  {
    "UNKNOWN STATE",
    "MAIN MENU",
    "AUTOMATIC-AUT",
    "PAS-A-PAS",
    "AUTOMATIC-EDT",
    "AUTOMATIC-REG",
    "AUTOMATIC_UAF",
    "PAS-A-PAS-PID",
    "PAS-A-PAS-STP",
    "PAS-A-PAS-REG",
    "PAS-A-PAS-UAF",
    "AUTOMATIC MODE DRE",
    "AUTOMATIC MODE NCO",
    "ALARME : FOUR OFF",
    "FAUTE : VOIE TEMPERATURE A ZERO",
    "FAUTE : ERREUR HORLOGE",
    "FAUTE : PROBLEME MESURE CAPTEUR",
    "FAUTE : TEMPERATURE SUPERIEURE TEMPERATURE ALARME",
    "FAUTE : PROBLEME RESISTANCE FOUR",
    "FAUTE : PROBLEME CALCUL PUISSANCE FOUR",
    "INITIALISATION PAGE REG EN COURS"
  };





  // ========================================================
  //---------------------------------------------------------  
  //- the YAT user messages 
  const size_t EXEC_LOW_LEVEL_MSG = yat::FIRST_USER_MSG + 1000;
  const size_t REG_MSG            = yat::FIRST_USER_MSG + 1001;
  const size_t PAP_MSG            = yat::FIRST_USER_MSG + 1002;
  const size_t RET_MSG            = yat::FIRST_USER_MSG + 1003;
  const size_t FIN_MSG            = yat::FIRST_USER_MSG + 1004;
  const size_t SET_SETPOINT_MSG   = yat::FIRST_USER_MSG + 1005;
  const size_t STOP_RAMP_MSG      = yat::FIRST_USER_MSG + 1006;
  const size_t SET_DEADBAND_MSG   = yat::FIRST_USER_MSG + 1007;
  const size_t INIT_GO_TO_RGU     = yat::FIRST_USER_MSG + 1008;
  const size_t SET_PUMP_MANUAL    = yat::FIRST_USER_MSG + 1009;
  const size_t SET_PUMP_AUTOMATIC = yat::FIRST_USER_MSG + 1010;
  const size_t SET_PUMP_SPEED     = yat::FIRST_USER_MSG + 1011;
  const size_t SET_REGUL_CHANNEL  = yat::FIRST_USER_MSG + 1012;
  const size_t SET_OVEN_MAX_POWER = yat::FIRST_USER_MSG + 1013;
 
  


  //- structures for use with messages service to write values in the BT500
  //- the structure to write a string 
  typedef struct LowLevelMsg
  {
    std::string cmd;
  }LowLevelMsg;

  
  //------------------------------------------------------------------------
  //- HWProxy Class
  //- read the HW 
  //------------------------------------------------------------------------
  class HWProxy : public yat4tango::DeviceTask
  {
    public :

  //- the configuration structure
  typedef struct Config
  {
    //- members
    std::string url;
    size_t startup_timeout_ms;
    size_t read_timeout_ms;
    size_t periodic_timeout_ms;
    time_t time_in_deadband;
    Tango::DevShort channel_in_use;

    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- Ctor -------------
    Config (std::string url );

    //- operator = -------
    void operator = (const Config & src);
  }Config;
   
    //- Constructeur/destructeur
    HWProxy (Tango::DeviceImpl * _host_device,
             Config & conf);

    virtual ~HWProxy ();

    //- the number of communication successfully done
    unsigned long get_com_error (void)   { return com_error; };
    //- the number of communication errors
    unsigned long get_com_success (void) { return com_success; };
    //- the number of commands successfully done
    unsigned long get_command_error (void)   { return command_error; };
    //- the number of command errors
    unsigned long get_command_success (void) { return command_ok; };
    //- the state and status of communication
    ComState get_com_state (void)        {return com_state; };
    std::string get_com_status (void)    { return hw_state_str [com_state]; };
    std::string get_last_error (void)    { return last_error; };

    //- access to (TC1 to TC4 and setpoint) temperatures
    double get_temperature (CommandSource cmd_source);

    //- access to regulation channel
    int get_regulation_channel (void)
    {
      return regulation_channel;
    };

    //- access to pump mode (manual = false, automatic = true
    bool get_pump_automatic_mode (void)
    {
      return pump_automatic_mode;
    };
    //- access to pump mode (manual = false, automatic = true
    int get_pump_speed (void)
    {
      return pump_speed;
    };
    //- access to pump mode (manual = false, automatic = true
    int get_oven_max_power (void)
    {
      return oven_max_power;
    };
    //- access to pump mode (manual = false, automatic = true
    int get_oven_power_current_percent (void)
    {
      return oven_power_current_percent;
    };


    //- Acces to deadband ramp utility
    double deadband (void) { return reg_manager->get_deadband (); };

    //- Access to State of the Ramp utility
    Tango::DevState check_ramp_state (void) { return reg_manager->get_state (); };

    //- Access to the Ramp Status 
    std::string check_ramp_status (void) { return reg_manager->get_status (); };

    BT500State get_bt_state (void) { return bt500_state; };
   //- gets the STA(tus) value
    long get_sta_value (void) { return sta_value; };




    //- sends a Low Level Command (do not check the syntax)  to the hard -blocking!
    std::string exec_low_level_command (std::string cmd);


  protected:
	  //- process_message (implements yat4tango::DeviceTask pure virtual method)
	  virtual void process_message (yat::Message& msg)throw (Tango::DevFailed);


  private :
  
    //- utilities
    yat::Mutex m_lock;
    
    //- create serial device_proxy
     void create_device_proxy (void);

    //- checks the BT STATE
    BT500State check_BT_state (void);
 
    //- get the temperature values
    void poll_hardware (void);

    //- sends a cmd to the hard -blocking!
    //- std::string write_read (std::string cmd);
    bool write_read (std::string cmd, std::string & response);

    //- The regulation (Ramp) State Manager
    rsm_ns::RampingStateMachine * reg_manager;


    //- the configuration
    Config conf;

    //- Tango communication device stuff
    Tango::DeviceProxyHelper * serial;

    //- the host device 
    Tango::DeviceImpl * host_dev;

    //- the periodic execution time
    size_t periodic_exec_ms;
 
    //- the state and status stuff
    ComState com_state;
    std::string com_status;

    std::string last_error;
    //- for Com error attribute
    unsigned long com_error;


    //- for Com OK attribute
    unsigned long com_success;

    //- internal error counter
    unsigned long consecutive_com_errors;

    //- for command error (command refused by BT500)
    unsigned long command_ok;
    unsigned long command_error;
    unsigned long consecutive_command_error;



    //- the value to read this turn
    int command_source;

    //- the BT state 
    long sta_value;
    BT500State bt500_state;


    double get_temp_value (std::string cmd);


    //- data
    double setpoint;
    double temperature_1;
    double temperature_2;
    double temperature_3;
    double temperature_4;

    //- pump in manual/automatic mode
    bool pump_automatic_mode;
    int pump_speed;

    //- the channel for regulation
    int regulation_channel;

    //- oven power management
    int oven_max_power;
    int oven_power_current_percent;



  };
}//- namespace
#endif //- __HW_PROXY_H__
